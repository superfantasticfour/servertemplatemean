var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var validatePresenceOf = function(value) {
  return (value && value.length);
};

var userSchema = new Schema({
    firstName : { type: String, required: true},
    lastName : { type: String, required: true, maxlength: 140},
    displayName : { type: String },
    password : { type: String, required: true, validate: [validatePresenceOf, 'Password cannot be blank'] },
    email: { type: String, required: true },
    photoURL: { type: String, required: true },
    salt: String
},
{collection: 'users'});

userSchema.virtual('fullname').get(function(){
    return this.firstName + " " + this.lastName;
});

userSchema.index({email : 1, type: -1})

// methods ======================
// generating a hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

var User = mongoose.model('User', userSchema);

module.exports = User;